﻿using System;
using System.Collections;
using System.Collections.Generic;
using TimerProject.Utils;
using UnityEngine;

namespace TimerProject.Core
{
    public class CoroutineDispatcher : Singleton<CoroutineDispatcher>
    {
        private readonly List<IEnumerator> _disposedCoroutines = new List<IEnumerator>();

        public IDisposable StartInternalTimer(Action timer)
        {
            var internalTimer = new InternalTimer(TimerCoroutine(timer));
            internalTimer.StartInternalCoroutine();
            return internalTimer;
        }

        private IEnumerator TimerCoroutine(Action timer)
        {
            while (true)
            {
                if (timer != null)
                {
                    timer();
                }
                yield return null;
            }
        }

        private void Update()
        {
            foreach (var disposedCoroutine in _disposedCoroutines)
            {
                StopCoroutine(disposedCoroutine);
            }
        }

        private class InternalTimer : IDisposable
        {
            private readonly IEnumerator _coroutine;

            public InternalTimer(IEnumerator coroutine)
            {
                _coroutine = coroutine;
            }

            public void StartInternalCoroutine()
            {
                Instance.StartCoroutine(_coroutine);
            }

            public void Dispose()
            {
                if (Instance != null) //TODO: fix Instance null exception for awake
                {
                    Instance._disposedCoroutines.Add(_coroutine);
                }
            }
        }
    }

}