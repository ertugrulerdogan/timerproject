﻿using System;
using System.Collections.Generic;
using TimerProject.Utils;
using UnityEngine;

namespace TimerProject.Core
{
    public enum TimerElementState
    {
        Ready,
        Paused,
        InProgress,
    }

    //TODO: Add callbacks for Element
    public sealed class TimerElement
    {
        public float Duration { get; private set; }
        public float Delay { get; private set; }
        public bool Concurrent { get; set; }

        private float _timer;
        private float _delay;
        private TimerElementState _elementState;

        private readonly List<Action> _subscribes = new List<Action>();
        private readonly Action<float> _task;
        private Action _onCompleted;
        private IDisposable _d;

        public TimerElement(float duration, Action<float> task, float delay = 0f)
        {
            Duration = duration;
            _task = task;
            Delay = delay;
        }

        public TimerElement(float duration, Action<float> task)
        {
            Duration = duration;
            _task = task;
        }

        public TimerElement(float duration)
        {
            Duration = duration;
        }

        public void Play(Action<TimerElement> onNext)
        {
            Play(() =>
            {
                if (onNext != null)
                {
                    onNext(this);
                }
            });
        }

        public void Play(Action onCompleted = null)
        {
            Dispose();
            Reset();

            _elementState = TimerElementState.InProgress;
            _onCompleted = onCompleted;
            _d = CoroutineDispatcher.Instance.StartInternalTimer(() =>
            {
                if (_elementState != TimerElementState.InProgress)
                {
                    return;
                }

                if (_delay > 0)
                {
                    _delay -= Time.deltaTime;
                }
                else
                {
                    _timer += Time.deltaTime / Duration;

                    if (_timer >= 1f)
                    {
                        Complete();
                    }
                    else if (_task != null)
                    {
                        _task(_timer);
                    }
                }
            });
        }

        public void Pause()
        {
            if (_elementState == TimerElementState.InProgress)
            {
                _elementState = TimerElementState.Paused;
            }
            else
            {
                Debug.LogError("Can't pause, timer not in progress");
            }
        }

        public void Resume()
        {
            if (_elementState == TimerElementState.Paused)
            {
                _elementState = TimerElementState.InProgress;
            }
            else
            {
                Debug.LogError("Can't resume, timer not paused");
            }
        }

        public void Stop(bool forceComplete = false)
        {
            if (forceComplete)
            {
                Complete();
            }

            Reset();
            _elementState = TimerElementState.Ready;
        }

        public void Restart(bool forceComplete = false)
        {
            if (forceComplete)
            {
                Complete();
            }

            Reset();
            _elementState = TimerElementState.InProgress;
        }

        public TimerHandle Subscribe(Action callback)
        {
            _subscribes.Add(callback);
            return new TimerHandle(this);
        }

        public TimerHandle UnSubscribe(Action callback)
        {
            if (_subscribes.Contains(callback))
            {
                _subscribes.Remove(callback);
            }

            return new TimerHandle(this);
        }

        public void Dispose()
        {
            if (_d != null)
            {
                _d.Dispose();
            }
        }

        private void Complete()
        {
            _elementState = TimerElementState.Ready;

            foreach (var subscribe in _subscribes)
            {
                subscribe();
            }

            if (_onCompleted != null)
            {
                _onCompleted();
            }
        }

        private void Reset()
        {
            _timer = 0f;
            _delay = Delay;
        }

        ~TimerElement()
        {
            Dispose();
        }
    }
}