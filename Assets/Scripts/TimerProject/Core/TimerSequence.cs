﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimerProject.Utils;

namespace TimerProject.Core
{
    public class TimerSequence
    {
        public event Action OnPlay;
        public event Action OnStop;
        public event Action OnPause;
        public event Action OnResume;
        public event Action OnComplete;

        public bool IsPlaying { get; private set; }

        //TODO: Add Restart for sequence 
        private readonly Queue<TimerElement> _timerElements = new Queue<TimerElement>();
        private readonly List<TimerElement> _inProgress = new List<TimerElement>();

        public SequenceHandle Add(TimerElement timer, bool concurrent = false)
        {
            timer.Concurrent = concurrent;
            _timerElements.Enqueue(timer);
            return new SequenceHandle(this);
        }

        public void Play()
        {
            if (IsPlaying)
            {
                return;
            }

            ProcessNext();

            if (OnPlay != null)
            {
                OnPlay();
            }

            IsPlaying = true;
        }

        public void Stop(bool completeAll = false)
        {
            if (completeAll)
            {
                CompleteAll();
            }
            else
            {
                foreach (var element in _inProgress)
                {
                    element.Stop();
                }
            }

            if (OnStop != null)
            {
                OnStop();
            }

            IsPlaying = false;
        }

        public void Pause()
        {
            foreach (var element in _inProgress)
            {
                element.Pause();
            }

            if (OnPause != null)
            {
                OnPause();
            }

            IsPlaying = false;
        }

        public void Resume()
        {
            foreach (var element in _inProgress)
            {
                element.Resume();
            }

            if (OnResume != null)
            {
                OnResume();
            }

            IsPlaying = true;
        }

        private void CompleteInprogress()
        {
            foreach (var element in _inProgress)
            {
                element.Stop();
            }
        }

        private void CompleteAll()
        {
            foreach (var element in _inProgress.Concat(_timerElements))
            {
                element.Stop();
            }
        }

        private void ProcessNext()
        {
            if (_timerElements.Any())
            {
                var t = _timerElements.Dequeue();
                t.Play(OnNext);
                _inProgress.Add(t);
                ProcessConcurrent();
            }
            else
            {
                if (OnComplete != null)
                {
                    OnComplete();
                }

                IsPlaying = false;
            }
        }

        private void ProcessConcurrent()
        {
            if (_timerElements.Any())
            {
                var t = _timerElements.Peek();
                if (t.Concurrent)
                {
                    _timerElements.Dequeue();
                    t.Play(OnNext);
                    _inProgress.Add(t);
                    ProcessConcurrent();
                }
            }
        }

        private void OnNext(TimerElement timer)
        {
            if (_inProgress.Contains(timer))
            {
                _inProgress.Remove(timer);
            }

            if (!timer.Concurrent)
            {
                ProcessNext();
            }
        }

        ~TimerSequence()
        {
            if (IsPlaying)
            {
                Stop();
            }
        }
    }
}