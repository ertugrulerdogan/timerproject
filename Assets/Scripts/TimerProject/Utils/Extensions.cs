﻿using System;
using TimerProject.Core;

namespace TimerProject.Utils
{
    public class TimerHandle
    {
        public TimerElement Timer;

        public TimerHandle(TimerElement timer)
        {
            Timer = timer;
        }
    }

    public class SequenceHandle
    {
        public TimerSequence Sequence;

        public SequenceHandle(TimerSequence sequence)
        {
            Sequence = sequence;
        }
    }

    public static class Extensions
    {
        #region TimerExtensions

        public static TimerElement Play(this TimerHandle handle, Action onCompleted = null)
        {
            handle.Timer.Play(onCompleted);
            return handle.Timer;
        }

        public static TimerElement Pause(this TimerHandle handle)
        {
            handle.Timer.Pause();
            return handle.Timer;
        }

        public static TimerElement Resume(this TimerHandle handle)
        {
            handle.Timer.Resume();
            return handle.Timer;
        }

        public static TimerElement Stop(this TimerHandle handle, bool forceComplete = false)
        {
            handle.Timer.Stop(forceComplete);
            return handle.Timer;
        }

        public static TimerElement Restart(this TimerHandle handle, bool forceComplete = false)
        {
            handle.Timer.Restart(forceComplete);
            return handle.Timer;
        }

        public static TimerHandle Subscribe(this TimerHandle handle, Action callback)
        {
            return handle.Timer.Subscribe(callback);
        }

        public static TimerHandle UnSubscribe(this TimerHandle handle, Action callback)
        {
            return handle.Timer.UnSubscribe(callback);
        }

        public static void Dispose(this TimerHandle handle)
        {
            handle.Timer.Dispose();
        }

        #endregion

        #region SequenceExtensions

        public static SequenceHandle Add(this SequenceHandle handle, TimerElement timer, bool concurrent = false)
        {
            return handle.Sequence.Add(timer, concurrent);
        }

        public static TimerSequence Play(this SequenceHandle handle)
        {
            handle.Sequence.Play();
            return handle.Sequence;
        }

        public static TimerSequence Stop(this SequenceHandle handle, bool completeAll = false)
        {
            handle.Sequence.Stop(completeAll);
            return handle.Sequence;
        }

        public static TimerSequence Pause(this SequenceHandle handle)
        {
            handle.Sequence.Pause();
            return handle.Sequence;
        }

        public static TimerSequence Resume(this SequenceHandle handle)
        {
            handle.Sequence.Resume();
            return handle.Sequence;
        }

        #endregion
    }
}