﻿using TimerProject.Core;
using TimerProject.Utils;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    private TimerElement _timer;

    private void Start()
    {
        _timer = new TimerElement(2f, f =>
        {
            Debug.Log(f);
        }).Subscribe(() =>
        {
            Debug.Log("s1");
        })
        .Subscribe(() =>
        {
            Debug.Log("s2");
        }).Timer;

        new TimerSequence().Add(_timer).Add(_timer).Play();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            _timer.Restart();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            _timer.Pause();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            _timer.Resume();
        }
    }
}
